/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo(){
		let name = prompt("What is your name?");
		let age =  prompt("How old are you?");
		let address =  prompt("Where do you live?");

		alert("Thank you for your inputs");

		console.log("Hello, " + name);
		console.log("You are " + age + " years old");
		console.log("You live in " + address);

	}
	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function getFavBands(){
		let band1 = "Bon Jovi";
		let band2 = "Guns N Roses";
		let band3 = "Linkin Park";
		let band4 = "I belong to the zoo";
		let band5 = "Kamikazee";
		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);

	}
	getFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function getFavMovies(){
		let movie1 = function(){
			let movieName = "Inception";
			let rottenTomatoRating = "87%";
			console.log("1. " + movieName);
			console.log("Rotten Tomatoes Rating: " + rottenTomatoRating);
		}
		movie1();
		let movie2 = function(){
			let movieName = "Taken 1";
			let rottenTomatoRating = "59%";
			console.log("2. " + movieName);
			console.log("Rotten Tomatoes Rating: " + rottenTomatoRating);
		} 
		movie2();
		let movie3 = function(){
			let movieName = "Taken 2";
			let rottenTomatoRating = "22%";
			console.log("3. " + movieName);
			console.log("Rotten Tomatoes Rating: " + rottenTomatoRating);
		} 
		movie3();
		let movie4 = function(){
			let movieName = "Taken 3";
			let rottenTomatoRating = "13%";
			console.log("4. " + movieName);
			console.log("Rotten Tomatoes Rating: " + rottenTomatoRating);
		} 
		movie4();
		let movie5 = function(){
			let movieName = "EDGE OF TOMORROW";
			let rottenTomatoRating = "91%";
			console.log("5. " + movieName);
			console.log("Rotten Tomatoes Rating: " + rottenTomatoRating);
		} 
		movie5();
	}
	getFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);